﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GDI_Painter
{
    public partial class Form1 : Form
    {
        Graphics _gc = null;
        VGA _render = null;

        Dictionary<String, bool> _keyMap = null;
        bool DRAWSTATE = false;
        int DRAWTYPE = 0;

        public Form1()
        {
            InitializeComponent();
            _gc = CreateGraphics();
            _gc.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            _gc.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

            _render = new VGA(ref _gc);

            _keyMap = new Dictionary<string, bool>();
            _keyMap.Add("Return", false);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.D.Equals(e.KeyCode))
            {
                if (_render.Layers.Count > 0)
                    _render.Layers.Remove(_render.Layers.Last());
                
                DRAWSTATE = false;
            }

            else if (_keyMap.ContainsKey(e.KeyCode.ToString()))
                _keyMap[e.KeyCode.ToString()] = true;
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (Keys.Return.Equals(e.KeyCode))
                DRAWSTATE = false;

            else if (Keys.L.Equals(e.KeyCode))
            {
                DRAWSTATE = false;
                DRAWTYPE = 0;
            }
            else if (Keys.C.Equals(e.KeyCode))
            {
                DRAWSTATE = false;
                DRAWTYPE = 1;
            }
            else if (Keys.R.Equals(e.KeyCode))
            {
                DRAWSTATE = false;
                DRAWTYPE = 2;
            }
            else if (_keyMap.ContainsKey(e.KeyCode.ToString()))
                _keyMap[e.KeyCode.ToString()] = false;
        }

        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            if (e.X <= 150)
                return;
   
            switch(DRAWTYPE)
            {
                case 0:
                    if (!DRAWSTATE)
                    {
                        DRAWSTATE = true;
                        _render.Layers.Add(new TypeLine("LayerLine" + _render.Layers.Count));
                        _render.Layers.Last().AddPoint(e.X, e.Y);
                    }
                    else _render.Layers.Last().AddPoint(e.X, e.Y);
                    break;

                case 1:
                    if (!DRAWSTATE)
                    {
                        DRAWSTATE = true;
                        _render.Layers.Add(new TypeCircle("LayerCircle" + _render.Layers.Count));
                        _render.Layers.Last().AddPoint(e.X, e.Y);
                    }
                    else
                    {
                        DRAWSTATE = false;
                        _render.Layers.Last().AddPoint(e.X, e.Y);
                    }
                    break;

                case 2:
                    if (!DRAWSTATE)
                    {
                        DRAWSTATE = true;
                        _render.Layers.Add(new TypeRect("LayerRect" + _render.Layers.Count));
                        _render.Layers.Last().AddPoint(e.X, e.Y);
                    }
                    else
                    {
                        DRAWSTATE = false;
                        _render.Layers.Last().AddPoint(e.X, e.Y);
                    }
                    break;

                default: break;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _render.Stop();
        }
    }
}
