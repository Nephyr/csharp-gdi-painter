﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;

namespace GDI_Painter
{
    interface Layer
    {
        void DrawLayer(ref Graphics _handler);
        void AddPoint(int _x, int _y);
        void DelPoint(int _x, int _y);
        void Move(int _x, int _y);
        String getName();
    }

    class VGA
    {
        Thread _handler = null;
        Graphics _gdi = null;

        public List<Layer> Layers = null;

        public VGA(ref Graphics _gc)
        {
            _handler = new Thread(_update);
            _handler.Start();

            _gdi = _gc;
            Layers = new List<Layer>();
        }

        public void Stop() { _handler.Abort(); }

        private void _update()
        {
            int textY = 10;

            while (true)
            {
                try
                {
                    //------------------------------------------------
                    //  VGA REFRESH
                    //------------------------------------------------
                    Thread.Sleep(50);
                    _gdi.Clear(Color.Black);

                    //------------------------------------------------
                    //  FRAME UPDATE
                    //------------------------------------------------
                    if (Layers.Count > 0)
                    {
                        for (int i = 0; i < Layers.Count; i++)
                            Layers.ToArray()[i].DrawLayer(ref _gdi);

                        _gdi.FillRectangle(new SolidBrush(Color.Black),
                                           new Rectangle(0, 0, 150, 600));
                        _gdi.DrawLine(new Pen(Color.White, 1), 150, 0, 150, 600);

                        for (int i = 0; i < Layers.Count; i++)
                        {
                            _gdi.DrawString(Layers.ToArray()[i].getName(),
                                           new Font("Arial", 12),
                                           new SolidBrush(Color.White),
                                           new PointF(15, textY));

                            textY += 24;
                        }
                    }

                    textY = 10;
                }
                catch (Exception) { }
            }
        }


    }
}
