﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GDI_Painter
{
    class TypeLine : Layer
    {
        String _name = "";
        Pen _color = null;
        List<Point> _points = null;

        public TypeLine(String _txt)
        {
            _name = _txt;
            _color = new Pen(Color.White, 2);
            _points = new List<Point>();
        }

        public String getName() { return _name; }

        public void DrawLayer(ref Graphics _handler)
        {
            for (int i = 0; i < _points.Count(); i++)
            {
                if (i == 0)
                    _handler.DrawLine(_color, _points.ToArray()[i], _points.ToArray()[i]);
                else
                    _handler.DrawLine(_color, _points.ToArray()[i - 1], _points.ToArray()[i]);

                _handler.DrawEllipse(new Pen(Color.Red, 3),
                                     _points.ToArray()[i].X-3,
                                     _points.ToArray()[i].Y-3,
                                     6, 6);
            }
        }

        public void AddPoint(int _x, int _y)
        {
            _points.Add(new Point(_x, _y));
        }

        public void DelPoint(int _x, int _y) { }
        public void Move(int _x, int _y) { }
    }
}
