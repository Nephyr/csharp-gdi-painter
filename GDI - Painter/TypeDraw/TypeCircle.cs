﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GDI_Painter
{
    class TypeCircle : Layer
    {
        String _name = "";
        Pen _color = null;
        List<Point> _points = null;

        public TypeCircle(String _txt)
        {
            _name = _txt;
            _color = new Pen(Color.Orange, 2);
            _points = new List<Point>();
        }

        public String getName() { return _name; }

        public void DrawLayer(ref Graphics _handler)
        {
            _handler.DrawEllipse(new Pen(Color.Red, 3),
                                 _points.ToArray()[0].X - 3,
                                 _points.ToArray()[0].Y - 3,
                                 6, 6);

            if(_points.Count == 2)
            {
                float x1 = _points.ToArray()[0].X;
                float y1 = _points.ToArray()[0].Y;

                float x2 = _points.ToArray()[1].X;
                float y2 = _points.ToArray()[1].Y;

                float dst = (float) Math.Sqrt(Math.Pow(Math.Abs(x2 - x1), 2) + Math.Pow(Math.Abs(y2 - y1), 2));
                dst *= 2;

                _handler.DrawEllipse(_color,
                                     _points.ToArray()[0].X - (dst / 2),
                                     _points.ToArray()[0].Y - (dst / 2),
                                     dst, dst);
            }
        }

        public void AddPoint(int _x, int _y)
        {
            if(_points.Count < 2)
                _points.Add(new Point(_x, _y));
        }

        public void DelPoint(int _x, int _y) { }
        public void Move(int _x, int _y) { }
    }
}
