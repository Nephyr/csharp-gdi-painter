﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GDI_Painter
{
    class TypeRect : Layer
    {
        String _name = "";
        Pen _color = null;
        List<Point> _points = null;

        public TypeRect(String _txt)
        {
            _name = _txt;
            _color = new Pen(Color.Orange, 2);
            _points = new List<Point>();
        }

        public String getName() { return _name; }

        public void DrawLayer(ref Graphics _handler)
        {
            _handler.DrawEllipse(new Pen(Color.Red, 3),
                                 _points.ToArray()[0].X - 3,
                                 _points.ToArray()[0].Y - 3,
                                 6, 6);

            if(_points.Count == 2)
            {
                float x1 = _points.ToArray()[0].X;
                float y1 = _points.ToArray()[0].Y;

                float x2 = _points.ToArray()[1].X;
                float y2 = _points.ToArray()[1].Y;

                _handler.DrawRectangle(_color, 
                                       (x1 < x2) ? x1 : x2,
                                       (y1 < y2) ? y1 : y2, 
                                       Math.Abs(x2 - x1),
                                       Math.Abs(y2 - y1));

                _handler.DrawEllipse(new Pen(Color.Red, 3),
                                     x2 - 3,
                                     y2 - 3,
                                     6, 6);
            }
        }

        public void AddPoint(int _x, int _y)
        {
            if(_points.Count < 2)
                _points.Add(new Point(_x, _y));
        }

        public void DelPoint(int _x, int _y) { }
        public void Move(int _x, int _y) { }
    }
}
